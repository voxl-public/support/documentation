---
layout: default
title: VOXL 2 Flight Deck Datasheet
parent: VOXL 2 Flight Deck
nav_order: 10
has_children: false
permalink: /voxl2-flight-deck-datasheet/
---

# VOXL 2 Flight Deck Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL 2 Flight Deck Configurations

| Part Number     | Description                                                       | 
|---              |---                                                                |
| MDK-F0006-1-01  | VOXL 2 Flight Deck with Microhard Modem Carrier Board and USB Hub |
| MDK-F0006-1-02  | VOXL 2 Flight Deck with 5G LTE Modem and USB Hub |
| MDK-F0006-1-03  | VOXL 2 Flight Deck with 2.4/5ghz WiFi Modem |

## High-Level Specs

| Specicifcation | [VOXL-2-FLIGHT-DECK](https://www.modalai.com/products/voxl-2-flight-deck) |
| --- | --- |
| Take-off Weight | Microhard: 144.4g <br> 5G LTE: 165.3g <br> WiFi: 148.2g |
| Computing | [VOXL 2 Flight](/voxl-2/) |
| Flight Control | PX4 on DSP |
| Image Sensors | [Stereo](/M0015/), [Tracking](/M0014/), [4k High-resolution](/M0025/) |
| Wireless Communication | Add-ons: Microhard, WiFi, or 5G LTE |
| Power Module | [Power Module v3](power-module-v3-datasheet) | 
| Battery | Up to 6S (sold separately), [buy here](https:/www.modalai.com/products/4s-battery-pack-gens-ace-3300mah) |

## Mechanical Drawings

The VOXL 2 Flight Deck 3D model can be found as a component of the Sentinel Drone [3D STEP](https://storage.googleapis.com/modalai_public/modal_drawings/Sentinel_Drone_STEP.zip) model.