---
layout: default
title: VOXL 2 Flight Deck Software 
parent: VOXL 2 Flight Deck User Guide
nav_order: 10
has_children: false
permalink: /voxl2-flight-deck-userguide-software/
---

# VOXL 2 Flight Deck Software 
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL 2 Flight Deck Software Overview

### ADB

To Set up ADB, see [Here](/setting-up-adb/)

Once adb is installed, a USB cable is plugged in, and your Flight Deck is powered on, open your terminal and run the following:

```bash
adb shell
```

You are now running a shell inside the VOXL 2's Ubuntu OS!

<hr>


### Software 
VOXL 2 Flight Deck runs VOXL SDK. For more information on configurations, calibrations, etc, see [Here](/voxl-sdk)

For information about using obstacle avoidance or VIO, see [Here](https://docs.modalai.com/voxl-vision-px4-collision-prevention/) and [Here](https://docs.modalai.com/flying-with-vio/)

<hr>

### Connectivity 

**5G LTE:** To install a SIM card and to connect your VOXL 2 Flight Deck to the internet, see [Here](/voxl2-networking-user-guide/) for an overview and [Here](/sentinel-user-guide-connect-gcs/#connecting-to-qgc-over-5g) for info on setting up a VPN service

**2.4/5ghz WiFi:** To set up WiFi on your VOXL 2 Flight Deck, see [Here](/voxl2-networking-user-guide/) for an overview and [Here](/voxl2-wifidongle-user-guide/) for instructions

**Microhard:** To set up a Microhard wireless connection, see [Here](/microhard-modems) for information and instructions 

<hr>

### Spektrum and RC settings

For Spektrum Binding and RC Instructions, see [Here](/sentinel-user-guide-pre-flight-setup/#rc-radio-setup)

<hr>
