---
layout: default
title: VOXL CAM User Guide
parent: VOXL CAM
has_children: True
nav_order: 10
permalink: /voxl-cam-user-guide/
---

# VOXL-CAM v1 User Guide
{: .no_toc }

[<img src="/images/voxl-cam/voxl-cam-front.png" width="50%" height="50%" style="
    float: left;">](#voxl-cam-core)
[<img src="/images/voxl-cam/voxl-cam-back.png" width="50%" height="50%" >](#voxl-cam-fully-integrated-robot-perception-system)

### If you have VOXL CAM with Flight Core or Modem, start by following the VOXL CAM Setup:

---

[VOXL CAM Setup](/voxl-cam-user-guide-core/){: .btn .btn-green }

