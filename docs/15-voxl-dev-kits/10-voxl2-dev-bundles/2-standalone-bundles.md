---
layout: default
title: VOXL 2 Standalone Bundles
parent: VOXL 2 Dev Bundles
nav_order: 2
has_children: false
permalink: /voxl2-standalone-bundles/
---

# VOXL 2 Standalone Bundles
{: .no_toc }




| Part Number                    | Description  | Purchase Link  |
|---                             |---           |--- |
| `MDK-M0054-1-00`               | VOXL 2 Dev Kit, Board Only |  [Buy Here](https://www.modalai.com/collections/voxl-development-kits/products/voxl-2?variant=39914779836467) |
| `MDK-M0054-1-01`               | VOXL 2 Dev Kit, Power Module and Supply, No Cameras  | [Buy Here](https://www.modalai.com/collections/voxl-development-kits/products/voxl-2?variant=39914779869235) |

### MDK-M0054-1-00

This kit contains the VOXL2 board only, and nothing else.  Volume users or users looking for a replacement would likely choose this development kit.

### MDK-M0054-1-01

This kit contains the VOXL2 board, along with the VOXL Power Module v3, power supply, and supporting cable.  Users looking to start out with development but don't need cameras are likely to choose this development kit.
