---
layout: default3
title: M0183 VOXL 2 mPCIe Expansion and USB Hub
nav_order: 0183
parent: Expansion Boards
has_children: true
permalink: /m0183/
buylink: https://www.modalai.com/products/m0183
summary: M0183 VOXL 2 mPCIe Expansion and USB Hub
---

# VOXL 2 mPCIe Expansion and USB Hub

{: .no_toc }

## Overview

The M0183 VOXL 2 mPCIe Expansion and USB Hub provides convenient access to extra I/O.

## Details

The M0183 provides a variety of basic access to several key features supported by the VOXL 2 platform, including:
- QTY. 1 Mini PCIe port
- QTY. 1 USB3 host port in the 10-pin ModalAI format with 2A VBUS support
- QTY. 2 USB2 host port in the 4-pin ModalAI format with 1A VBUS support
- QTY. 1 6-pin JST GH for SPI port access