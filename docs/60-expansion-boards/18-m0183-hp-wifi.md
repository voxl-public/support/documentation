---
layout: default3
title: High Power WiFi Module
nav_order: 11
parent: Expansion Boards
has_children: true
permalink: /m0183/
summary: This Expansion Adapter provides PCI connection as well as UART, I2C and USB
nav_exclude: true
search_exclude: true
---

# High Power WiFi Module (M0183)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview

The M0183 High Power WiFi Extension Board:

- 2x USB2 Type A Female connection and 1x USB3 connection
- An apps proc UART and I2C (VOXL 2 Only)
- SPI connection and addtional UART and I2C (On J8 if needed over PCI)
- Momentary fastboot switch
- PCI connection
 
<img src="/images/expansion-boards/m0183/M0183-connector-callouts_front.jpg" alt="m0141-top" width="620"/>

<img src="/images/expansion-boards/m0183/M0183-connector-callouts_back.jpg" alt="m0141-bottom" width="660"/>

## Connector Callouts

### P3 - Connects to J3 on Voxl 2
### P5 - Connects to J5 on Voxl 2
### J1 - Fan Connect

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5V Local             |                           |
| 2    | GND                  |                           |

### J2 - mPCIe
Supports 8devices miniPini
- 2041119-2 TE Connectivity 

### J9 - UART and I2C Connect
- Board Connector: JST SM06B-GHS-TB
- Mating Connector: GHR-09V-S

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 3.3V                 |                           |
| 2    | UART_TX              |           `/dev/ttyHS2`   |
| 3    | UART_RX              |           `/dev/ttyHS2`   |
| 4    | SDA                  |           `/dev/i2c-2`    |
| 3    | SCL                  |           `/dev/i2c-2`    |
| 4    | GND                  |                           |

### J10 - USB 2 Connect
- Board Connector: JST SM04B-GHS-TB
- Mating Connector: GHR-04V-S

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5P0V                 |                           |
| 2    | USB_N                |                           |
| 3    | USB_P                |                           |
| 4    | GND                  |                           |


### J11 - USB 3.0 Connect
- Board Connector: JST SM10B-GHS-TB
- Mating Connector: GHR-10V-S

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5P0V                 |                           |
| 2    | USB_N                |                           |
| 3    | USB_P                |                           |
| 4    | GND                  |                           |
| 5    | RX_N                 |                           |
| 6    | RX_P                 |                           |
| 7    | GND                  |                           |
| 8    | TX_N                 |                           |
| 9    | TX_P                 |                           |
| 10   | GND                  |                           |


### J12 - USB 2 Connect
- Board Connector: JST SM04B-GHS-TB
- Mating Connector: GHR-04V-S

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5P0V                 |                           |
| 2    | USB_N                |                           |
| 3    | USB_P                |                           |
| 4    | GND                  |                           |


### J13 - Micro-SD Card Slot

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | 5P0V                 |                           |
| 2    | USB_N                |                           |
| 3    | USB_P                |                           |
| 4    | GND                  |                           |


### SW3 

Force Fastboot momentary button. 

