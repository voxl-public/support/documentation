---
layout: default
title: VOXL 2 USB3 UART Add-On Datasheet
parent: VOXL 2 USB3 UART Add-On
nav_order: 10
has_children: false
permalink: /voxl2-usb3-uart-add-on-datasheet/
---

# VOXL 2 USB3 UART Add-On Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview
 
<img src="/images/voxl2-usb3-uart-add-on/m0125-hero.jpg" alt="m0125-hero" width="1280"/>

## Dimensions

### 3D Drawings

[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0125_A.step)

### 2D Drawings

TODO
<!--
![flight-core-dims.png](/images/flight-core/flight-core-dims.png)
![flight_core_v1_imu_locations.png](/images/flight-core/flight_core_v1_imu_locations.png)
-->

## Features

- Exposes an apps proc UART (`/dev/ttyHS1` on VOXL2, `/dev/ttyHS4` on RB5) on a 4-pin JST connector
- Exposes a USB3 on a 10-pin JST connector

## Schematic

[M0125 Schematic](https://storage.googleapis.com/modalai_public/modal_schematics/M0125_A_SCHEMATIC.pdf)

## Connector Callouts

<!--
<img src="/images/voxl2-usb3-uart-add/m0125-connectors-v0.png" alt="m0125-connectors" width="1280"/>
-->
<hr>

### J3 - UART

| Connector | MPN |
| --- | --- | 
| Board Connector | BM04B-GHS-TBT(LF)(SN)(N) |
| Mating Connector |

| Pin #      | Signal Name           | Usage / Notes                                 |
|---         |---                    |---                                            |
| 1          | 3P3V                  |                                               |
| 2          | UART7_RX_3P3V         | `/dev/ttyHS1`                                 |
| 3          | UART7_TX_3P3V         | `/dev/ttyHS1`                                 |
| 4          | GND                   |                                               |

<hr>

### J2 - USB3

| Connector | MPN |
| --- | --- | 
| Board Connector | BM10B-GHS-TBT(LF)(SN)(N) |
| Mating Connector |

| Pin# | Signal               | Notes/Usage               |
|------|----------------------|---------------------------|
| 1    | VBUS 5P0V            |                           |
| 2    | USB1_HS_CONN_D_N     |                           |
| 3    | USB1_HS_CONN_D_P     |                           |
| 4    | GND                  |                           |
| 5    | VOXL2_USB1_SS_RX_N   |                           |
| 6    | VOXL2_USB1_SS_RX_P   |                           |
| 7    | GND                  |                           |
| 8    | VOXL2_USB1_SS_TX_N   |                           |
| 9    | VOXL2_USB1_SS_TX_P   |                           |
| 10   | GND                  |                           |

<hr>
