---
layout: default
title: M0157 VOXL 2 Lepton Adapter
parent: Image Sensor Flex Cables and Adapters
nav_order: 157
has_children: false
permalink: /m0157/
---

# M0157 VOXL 2 Lepton Adapter
{: .no_toc }


Note: The M0157 is likely to be replaced with a slightly modified version which includes an i2c reset to reset the Lepton on demand.

## Overview


## VOXL SDK Requirements


## Hardware Requirements

### Connectors

#### J1 

Connects to [M0173](/M0173/) J6

| Pin | Function           |
|-----|--------------------|
| 1   | VREG_AVDD_2P8_FLIR |
| 2   | FLIR_SPI_SCK_2P8   |
| 3   | FLIR_SPI_CS_N_2P8  |
| 4   | FLIR_SPI_MISO_2P8  |
| 5   | I2C_SCL_2P8V       |
| 6   | I2C_SDA_2P8V       |
| 7   | CLK_25M_FLIR_2P8V  |
| 8   | DGND               |
