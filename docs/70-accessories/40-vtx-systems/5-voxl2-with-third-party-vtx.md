---
layout: default
title: VOXL2 with Third Party VTX
parent: VTX Systems
nav_order: 5
has_children: false
permalink: /voxl2-with-third-party-vtx/
---

# VOXL2 with Third Party VTX
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## VOXL2 / HDZero Freestyle 2 Integration Guide

### Hardware

Using [M0125](/voxl2-usb3-uart-add-on/) addon board.

<img src="/docs/70-accessories/40-vtx-systems/images/M0054-vtx-M0125-hdzero-v2.png">

Using [M0130](/lte-io-breakout-usb-hub-datasheet/) addon board.

<img src="/docs/70-accessories/40-vtx-systems/images/M0054-vtx-M0130-hdzero.png">

### Software

- VOXL SDK 1.2.1 or newer required
- For PX4 parameters, please reference [v1.14/other_helpers/msp_dp_osd.params](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-px4-params/-/blob/master/params/v1.14/other_helpers/msp_dp_osd.params) for required params
- Set `OSD` to `HDZERO` in `/usr/bin/voxl-px4`


## VOXL2 / Caddx Integration Guide

### Hardware

Using [M0125](/voxl2-usb3-uart-add-on/) addon board.

<img src="/docs/70-accessories/40-vtx-systems/images/M0054-vtx-M0125-caddx-v2.png">

Using [M0130](/lte-io-breakout-usb-hub-datasheet/) addon board.

<img src="/docs/70-accessories/40-vtx-systems/images/M0054-vtx-M0130-caddx.png">

### Software

- VOXL SDK 1.2.1 or newer required
- Set `OSD` to `DJI` in `/usr/bin/voxl-px4`

## VOXL2 / Vista Integration Guide 

### Hardware

TODO

### Software
- VOXL SDK newer than 1.3.2 (1.3.3+)
- For normal DJI OSD interface set `OSD` to `DJI` in `/usr/bin/voxl-px4`
- For MSP DisplayPort OSD interface set `OSD` to `DJI_MSP_DP` in `/usr/bin/voxl-px4`