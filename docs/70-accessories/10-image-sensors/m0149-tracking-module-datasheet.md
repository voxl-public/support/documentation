---
layout: default
title: M0149 Tracking Module Datasheet
parent: Image Sensors
nav_order: 149
has_children: false
permalink: /M0149/
---

# VOXL Tracking Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![M0149.JPG](/images/other-products/image-sensors/M0149_angle_1920x1920.jpg)

Tracking 1280x800 global-shutter sensor and flex cable to connect to VOXL 2® for indoor flight, GPS-denied navigation VIO and global shutter computer vision processing.

This module is based on an OnSemi AR0144 1280x800, global-shutter CMOS sensor with fisheye lens.



## Specification

### MSU-M0149-1-01 OnSemi AR0144 166° ([Buy Here](https://www.modalai.com/products/msu-m0149-1))


| Specification                   | Value                   |
|---------------------------------|-------------------------|
| Sensor                          | OnSemi AR0144           |
| Shutter                         | Global                  |
| Resolution                      | 1280x800                |
| Lens Size                       | 4.5mm                   |
| Lens Mount                      | M7 * P0.35              |
| Fov(DxHxV)                      | 162° x 132° 82°         |
| Effective Focal Length          | 1.7mm                   |
| F. NO                           | 2.4                     |
| Distortion                      | -31.79%                 |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0149-AR0144.STEP)

#### 2D Diagram
[Download](https://storage.googleapis.com/modalai_public/modal_drawings/M0149_2D_02-24-24.pdf)

## Module Connector Pinout 

| Pin # | Signal Name            | Pin # | Signal Name            |
|-------|------------------------|-------|------------------------|
| 1     | DGND                   | 2     | DGND                   |
| 3     | NC                     | 4     | NC                     |
| 5     | SDA                    | 6     | VDDIO1.8/2.8V          |
| 7     | SCL                    | 8     | NC                     |
| 9     | RST_0                  | 10    | XCLK                   |
| 11    | DGND                   | 12    | DGND                   |
| 13    | MCP                    | 14    | FLASH                  |
| 15    | MCN                    | 16    | NC                     |
| 17    | MDP0                   | 18    | NC                     |
| 19    | MDN0                   | 20    | AVDD2.8V               |
| 21    | DGND                   | 22    | AGND                   |
| 23    | MDP1                   | 24    | NC                     |
| 25    | MDN1                   | 26    | TRIGGER                |
| 27    | NC                     | 28    | NC                     |
| 29    | NC                     | 30    | NC                     |
| 31    | DGND                   | 32    | DGND                   |
| 33    | NC                     | 34    | NC                     |
| 35    | NC                     | 36    | DGND                   |

[Pinout Image](https://storage.googleapis.com/modalai_public/modal_drawings/M0149pinoutImages.pdf)

## VOXL Integration

## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor

### Outdoor

## Hardware Design Guidance