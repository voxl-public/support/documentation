---
layout: default
title: M0113 OV9782 Module Datasheet
parent: Image Sensors
nav_order: 113
has_children: false
permalink: /M0113/
---

# VOXL Tracking Sensor Datasheet

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

![MSU-M0113picture.jpg](/images/other-products/image-sensors/MSU-M0113picture.jpg)

The M0113-1 is an RGB, global shutter, stereo sensor compatible with ModalAI's [M0010-1](m0010) and [M0039-1](m0039) stereo flex cables.

The M0113-1 does NOT incorporate an IR-cut filter.

The M0113-1 is identical to [M0073-1](/m0073/) except that M0113-1 does NOT incorporate an IR-cut filter.

This sensor is in production and is no longer recommended for new designs. 

## Specification

### MSU-M0113-1 OV9782 121° FOV 


| Specification | Value                   |
|----------------|-------------------------|
| Sensor         | OV9782 [Datasheet](https://www.ovt.com/wp-content/uploads/2022/01/OV9782-PB-v1.0-WEB.pdf)                 |
| Shutter        | Global Shutter          |
| Resolution     | 1280x800                |
| Focal Length   | 1.56 +/- 5%             |
| F Number       | 2.2 +/- 5%              |
| Fov(DxHxV)     | 121.3° x 103.2° x 84.6° |
| TV Distortion  | 10%                     |
| IR Filter      | No IR-cut Filter        |

### Requirements
### Current/Power Consumption

## Technical Drawings   

#### 3D STEP File
#### 2D Diagram


## Module Connector Pinout 
Module connector Molex 503776-2410

| Pin # | Signal Name |
|-------|-------------|
| 1     | DGND        |
| 2     | XCLK        |
| 3     | DGND        |
| 4     | XSHUTDOWN   |
| 5     | STROBE      |
| 6     | DGND        |
| 7     | MDP         |
| 8     | MDN         |
| 9     | DGND        |
| 10    | MCP         |
| 11    | MCN         |
| 12    | DGND        |
| 13    | ULPM        |
| 14    | FSIN        |
| 15    | NC          |
| 16    | DGND        |
| 17    | SIO_D       |
| 18    | SIO_C       |
| 19    | DGND        |
| 20    | DOVDD1.8V   |
| 21    | NC          |
| 22    | DGND        |
| 23    | AVDD2.8V    |
| 24    | AGND        |

## VOXL Integration

## VOXL SDK
### Camera Server Configuration
### VOXL Portal

## Image Samples for Sensor
### Indoor
### Outdoor

## Hardware Design Guidance