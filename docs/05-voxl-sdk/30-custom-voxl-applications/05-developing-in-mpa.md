---
title: Developing in Modal Pipe Architecture
layout: default
parent: Custom VOXL Applications
has_children: true
nav_order: 05
permalink: /mpa-development/
---

# Developing in MPA (Modal Pipe Architecture)

**Before reading this page, see [The Modal Pipe Architecture](/mpa/) page!** 

This page will discuss VOXL's software architecture in more detail, providing critical knowledge for any developer that would like to make custom applications for ROS.

{: .alert .simple-alert}
Note: If you would simply like to use ROS on VOXL, see the page on [Custom ROS Applications](/ros/).

## Overview

Topics Covered:
- libmodal pipe
- 