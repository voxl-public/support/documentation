---
title: Figure 8
layout: default
parent: Custom VOXL Applications
has_children: true
nav_order: 45
permalink: /voxl-figure-8/
---


# Figure Eight as an Example Custom VOXL SDK Application
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Overview


[voxl-vision-hub](/voxl-vision-hub/) is the core of the VOXL SDK interacting with the [internal](/voxl-px4/) or [external](/voxl2-external-flight-controller/) flight controller. [voxl-vision-hub](/voxl-vision-hub/) has an example autonomous indoor path following example called figure eight. Please get comfortable using this mode prior to developing custom code.

Once you are ready to make changes, you can use the [figure eight source code](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-hub/-/blob/master/src/offboard_figure_eight.c) as a template for your development. 


