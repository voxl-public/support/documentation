---
title: Connectivity
layout: default
parent: VOXL PX4
has_children: true
nav_order: 17
permalink: /voxl-px4-connectivity/
---

# PX4 connectivity

PX4 uses the [Mavlink protocol](https://mavlink.io/en/messages/common.html) to communicate
to the outside world (Including QGroundControl). Generally this is handled over an
IP link when using VOXL2. PX4 on VOXL2 utilizes a custom Mavlink router called `voxl-mavlink-server`
to route all Mavlink traffic correctly.

## voxl-mavlink-server Daemon

### Enable Daemon

`voxl-px4` communicates over IP to the outside world using the `voxl-mavlink-server` daemon.  To enable, run the following:

```
voxl-configure-mavlink-server factory_enable
```

*Example output from running command for reference:*

```
voxl2-mini:/$ voxl-configure-mavlink-server factory_enable
wiping old config file
Created new json file: /etc/modalai/voxl-mavlink-server.conf
enabling  voxl-mavlink-server systemd service
Done configuring voxl-mavlink-server
```

### IP Connection

- To get an IP link on VOXL 2, see [here](/voxl2-wifidongle-user-guide/)
- To get an IP link on VOXL 2 Mini, see [here](/voxl2-mini-usb-connections/) for options to get a USB connection going, 
and use some network adapter like a WiFi dongle (e.g. `awus036acs`).

Note: the adb connection does not pass IP traffic and can't be used for this link.

### GCS Connection using WiFi - Using SoftAP

If using a WiFi dongle, an easy way to get a connection going is to setup VOXL 2 / VOXL 2 Mini as a 
Soft AP (access point) and have your host computer connect wirelessly to it.

When in this mode, VOXL 2 Mini will provide your host PC an address of `192.168.8.10` by default.

The default IP address `voxl-mavlink-server` will use for the main connection to QGC.

So by default, a connection between VOXL 2 / VOXL 2 Mini and your GCS should be facilitated in this mode.

### GCS Connection using WiFi Station Mode or Ethernet Adapter

If using WiFi in Station Mode or an Ethernet Adapter, connect VOXL 2 / VOXL 2 Mini to the same network as your 
QGC and update the configuration file's `primary_static_gcs_ip` field at `/etc/modalai/voxl-mavlink-server.conf`

## Differences

When using PX4 on VOXL2 it is important to note a few differences to other
micro-controller based platforms such as Pixhawk.
- You cannot connect to QGroundControl via the USB-C port
- The Mavlink console in QGroundControl does not work with VOXL2
- Software update is not handled via QGroundControl. It is done via Debian packages.
