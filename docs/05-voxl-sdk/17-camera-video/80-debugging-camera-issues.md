---
title: Debugging Camera Issues
layout: default
parent: Cameras & Video
has_children: true
nav_order: 80
permalink: /debugging-camera-issues/
---

## Checking which cameras are detected by Camera Server

- stop the background service for camera server if it is running

```
systemctl stop voxl-camera-server
```

- run the command to list detected cameras

```
voxl2:/$ voxl-camera-server -l
DEBUG:   Attempting to open the hal module
DEBUG:   SUCCESS: Camera module opened on attempt 0
DEBUG:   ----------- Number of cameras: 3

DEBUG:   Cam idx: 0, Cam slot: 0, Slave Address: 0x0020, Sensor Id: 0x0214
DEBUG:   Cam idx: 1, Cam slot: 1, Slave Address: 0x0030, Sensor Id: 0x0356
DEBUG:   Cam idx: 2, Cam slot: 2, Slave Address: 0x0034, Sensor Id: 0x0577
DEBUG:   Note: This list comes from the HAL module and may not be indicative
DEBUG:   	of configurations that have full pipelines

DEBUG:   Number of cameras: 3
```

## Diagnosing a missing camera issue

- check whether the number of detected cameras is correct (from step above)
- match the Sensor Id to Sensor Type (see below) and verify that cameras have been correctly identified in their respective slots
  - see https://docs.modalai.com/voxl2-camera-configs/#overview for the mapping from physical connector to slot number
- if there is a camera that is not detected but appears to be plugged in
  - troubleshoot physical / connection issue:
    - double check physical connections
    - try to user a different flex cable if possible
    - try to use a different camera of the same type if possible
    - re-run the above camera detection test
  - troubleshoot camera driver configuration issue
    - for every camera, there needs to be a `sensormodule` driver present in `/usr/lib/camera/`
    - every sensormodule file has a slot number at the end, which needs to match the actual slot number that the camera is connected to
    - all of the sensormodule drivers are stored in `/usr/share/modalai/chi-cdk` and can be manually copied to `/usr/lib/camera` when doing manual camera setup
    - for example, if we have IMX214 plugged into slot 0, AR0144 in slot 1 and IMX412 in slot 2, the following sensormodules should be present in `/usr/lib/camera`
    ```
    com.qti.sensormodule.imx214_0.bin
    com.qti.sensormodule.ar0144_1.bin
    com.qti.sensormodule.imx412_2.bin
    ```
    - removing unused sensormodules is recommended to avoid confusion (not required)

## Sensor Id to Sensor Type Mapping
```
- 0x7750 : OV7251
- 0x9281 : OV9782 / OV9281 / OV9282
- 0x6442 : OV64B40
- 0x0214 : IMX214
- 0x0356 : AR0144
- 0x0577 : IMX412
- 0x0600 : IMX664 (this is a workaround for WHOAMI register reading 0)
- 0xA602 : IMX678
- 0x003D : IRS1645 (TOF V1)
- 0x2975 : IRS2975 (TOF V2)
- 0x00FF : Boson (this is a workaround)
```

## Diagnosing incorrect Camera Server config

- camera server configuration is stored in `/etc/modalai/voxl-camera-server.conf`
- the camera id in the config file should match the camera id of each camera detected using `voxl-camera-server -l`
- if there is a missing camera, the camera server will still attempt to run but it may incorrectly guess which camera is missing, resulting in wrong camera name assigned to a stream
- make sure that requested resolution does not exceed the maximum resolution of each camera (supported resolutions are also listed in output of `voxl-camera-serverl -l`)

