---
title: Core Services 
layout: default
parent: VOXL SDK
has_children: true
nav_order: 20
permalink: /mpa-services/
---

The MPA section outlines the architecture of VOXL's services and lists  the dozens of services available.

In this section, we dive into just a few critical services that are more than meets the eye. We will discuss how they work and how to use them.

**We highly recommend reading this section for a full understanding of VOXL's capabilities.**