---
title: Depth From Stereo 
layout: default
parent: Feature Guides
has_children: true
nav_order: 20
permalink: /voxl-dfs-server/
---

# Depth From Stereo
{: .no_toc }

voxl-dfs-server is a background systemd service that subscribes to a stereo image feed and outputs depth information via three MPA pipes: dfs_disparity, dfs_disparity_scaled, and dfs_point_cloud.

![geometry_overview](/images/voxl-sdk/voxl-portal/voxl-portal-dfs.png)

---
## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## Output Format

The data format is unique to each output pipe:

dfs_disparity:
Grayscale image with pixel values corresponding to calculated disparity. Each pixel will be within the range [0, `n_disparity_levels`], which can be changed in the configuration file.

dfs_disparity_scaled:
Grayscale image as above, but pixel values will correspond to scaled disparity values in the range of [0, 255].

dfs_point_cloud:
An XYZ pointcloud (as specified by [libmodal_pipe](/libmodal-pipe/) in `modal_pipe_interfaces.h`) with the reprojected distances.

For an example of how to read this data, see [voxl-inspect-points](/voxl-inspect-points/).


## Configuration

APQ8096 (VOXL1) and QRB5165 (VOXL2) based platforms have different implementations of the DFS algorithm. See the following child pages for platform-specific configuration.


## Visualization Options

You can view either the regular disparity or scaled image streams as another normal camera pipe. As such, it can be viewed with [voxl-portal](voxl-portal), converted to ROS with [voxl_mpa_to_ros](/voxl-mpa-to-ros/), and logged to disk with [voxl-logger](/voxl-logger/).


The pointcloud can be viewed with [voxl-portal](voxl-portal).

