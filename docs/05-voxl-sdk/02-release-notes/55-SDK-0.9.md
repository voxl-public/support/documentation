---
title: SDK 0.9 Release Notes
layout: default
parent: VOXL SDK Release Notes
has_children: false
nav_order: 55
permalink: /sdk-0.9-release-notes/
---

# SDK 0.9 Release Notes


## V0.9.5

This is a maintenance release for SDK-0.9

```
- More robust writing to pipes
- Allow mavlink 1 packets through libmodal_pipe and voxl-mavlink-server
- voxl-mavlink-server can auto-select between 2 uart ports on APQ8096
- fix 3D map bug in voxl-portal
```

| Package | Version | APQ8096 | QRB5165|
|:---|:---:|:---:|:---:|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server) | 0.3.1 (unchanged) | ✅ |  |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server) | 1.0.3 (unchanged) | ✅ |  |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng) | 1.6.38-1 (unchanged) | ✅ |  |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.3 (unchanged) | ✅ |  |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks) | 0.1.3 (unchanged) | ✅ |  |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite) | 2.8.3-1 (unchanged) | ✅ |  |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io) | 0.6.0 (unchanged) | ✅ |  |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv) | 0.2.3 (unchanged) | ✅ | ✅ |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure) | 0.0.7 (unchanged) | ✅ | ✅ |
| [libmodal-journal](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-journal) | 0.2.1 (unchanged) | ✅ | ✅ |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json) | 0.4.3 (unchanged) | ✅ | ✅ |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe) | 2.6.0 --> 2.8.2 | ✅ | ✅ |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) | 0.1.0 (unchanged) |  | ✅ |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math) | 1.3.0 (unchanged) | ✅ | ✅ |
| [libvoxl-cci-direct](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cci-direct) | 0.1.5 (unchanged) | ✅ | ✅ |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils) | 0.1.1 (unchanged) | ✅ | ✅ |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/mavlink-camera-manager) | 0.1.0 (unchanged) | ✅ |  |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server) | 0.1.0 (unchanged) |  | ✅ |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server) | 0.5.0 (unchanged) |  | ✅ |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks) | 0.1.5 (unchanged) |  | ✅ |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite) | 2.8.0-2 (unchanged) |  | ✅ |
| [voxl-bind](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind) | 0.0.1 (unchanged) |  | ✅ |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) | 0.2.3 (unchanged) | ✅ | ✅ |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server) | 1.3.5 (unchanged) | ✅ | ✅ |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver) | 1.14.0-9 (unchanged) | ✅ | ✅ |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor) | 0.3.0 (unchanged) | ✅ | ✅ |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support) | 1.2.4 (unchanged) | ✅ | ✅ |
| [voxl-eigen3](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-eigen3) | 3.4.0 (unchanged) | ✅ | ✅ |
| [voxl-flir-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-flir-server) | 0.2.0 (unchanged) | ✅ | ✅ |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server) | 0.0.10 (unchanged) | ✅ | ✅ |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo) | 2.1.3-4 (unchanged) | ✅ | ✅ |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc) | 1.0.7 (unchanged) | ✅ | ✅ |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger) | 0.3.4 (unchanged) | ✅ | ✅ |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper) | 0.1.5 (unchanged) | ✅ | ✅ |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink) | 0.1.0 (unchanged) | ✅ | ✅ |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) | 0.2.0 --> 0.3.0 | ✅ | ✅ |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) | 0.16.1 (unchanged) | ✅ | ✅ |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose) | 7.7.0-1 (unchanged) | ✅ | ✅ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools) | 0.7.6 (unchanged) | ✅ | ✅ |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) | 0.3.6 (unchanged) | ✅ | ✅ |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt) | 2.5.0-4 (unchanged) | ✅ | ✅ |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv) | 4.5.5-1 (unchanged) | ✅ | ✅ |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) | 0.4.2 --> 0.5.0 | ✅ | ✅ |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4) | 1.12.31 (unchanged) |  | ✅ |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server) | 0.1.2 (unchanged) |  | ✅ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server) | 0.8.2 (unchanged) | ✅ | ✅ |
| [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id) | 0.0.5 (unchanged) |  | ✅ |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-streamer) | 0.4.1 (unchanged) | ✅ | ✅ |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server) | 0.3.1 (unchanged) | ✅ | ✅ |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils) | 1.2.2 (unchanged) | ✅ | ✅ |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server) | 0.1.3 (unchanged) | ✅ | ✅ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-px4) | 1.4.0 (unchanged) | ✅ | ✅ |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox) | 1.1.3 (unchanged) | ✅ | ✅ |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn) | 0.0.6 (unchanged) | ✅ |  |


## V0.9.4

Requires VOXL (APQ8096) system image >= v4.0.0 and VOXL2/RB5F system image 1.4.1+

Major features:

```
IMU
 * Improve stability of ICM42688 driver
 * New Timestamp Filter module in librc_math
 * improved timestamps in qrb5165-imu-server
 * qrb5165-imu-server publishes live FFT of IMU data
 * enable AAF filter for ICM42688 accel on QRB5165
Portal
 * improve robustness un poor network conditions
 * Image framerate and quality reacts more quickly
 * new live FFT plot (qrb5165 only)
 * better CPU and IMU plots
Camera
 * PMD TOF support on both APQ8096 and QRB5165
Other
 * VIO level calibration
 * voxl-flir-server for Lepton 3 on both platforms
 * VIO packet includes cam_id field for feature points
 * point cloud packet includes ID and source fields
 * QRB5165 more robust pipe disconnection detection
 * voxl-mapper overhaul
 * QRB5165 support external UART flight controller in voxl-mavlink-server
 * VOA fallback mode when no VIO or attitude data is present
 * QVIO quality metric based on distribution of features
 * Preliminary (Beta) RemoteID support on QRB5165
```


| Package | Version | APQ8096 | QRB5165|
|:---|:---:|:---:|:---:|
| [apq8096-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-dfs-server) | 0.3.0 --> 0.3.1 | ✅ |  |
| [apq8096-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-imu-server) | 1.0.2 --> 1.0.3 | ✅ |  |
| [apq8096-libpng](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-libpng) | 1.6.38-1 (unchanged) | ✅ |  |
| [apq8096-rangefinder-server](https://gitlab.com/voxl-public/voxl-sdk/services/apq8096-rangefinder-server) | 0.1.2 --> 0.1.3 | ✅ |  |
| [apq8096-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/apq8096-system-tweaks) | 0.1.2 --> 0.1.3 | ✅ |  |
| [apq8096-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/apq8096-tflite) | 2.2.3-4 --> 2.8.3-1 | ✅ |  |
| [libapq8096-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libapq8096-io) | 0.5.8 --> 0.6.0 | ✅ |  |
| [libmodal-cv](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-cv) | 0.1.0 --> 0.2.3 | ✅ | ✅ |
| [libmodal-exposure](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-exposure) | 0.0.7 (unchanged) | ✅ | ✅ |
| [libmodal-journal](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-journal) | 0.2.1 (new) | ✅ | ✅ |
| [libmodal-json](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-json) | 0.4.2 --> 0.4.3 | ✅ | ✅ |
| [libmodal-pipe](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libmodal-pipe) | 2.4.0 --> 2.6.0 | ✅ | ✅ |
| [libqrb5165-io](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libqrb5165-io) | 0.1.0 (unchanged) |  | ✅ |
| [librc-math](https://gitlab.com/voxl-public/voxl-sdk/core-libs/librc-math) | 1.1.8 --> 1.3.0 | ✅ | ✅ |
| [libvoxl-cci-direct](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cci-direct) | 0.1.5 (new) | ✅ | ✅ |
| [libvoxl-cutils](https://gitlab.com/voxl-public/voxl-sdk/core-libs/libvoxl-cutils) | 0.1.1 (unchanged) | ✅ | ✅ |
| [mavlink-camera-manager](https://gitlab.com/voxl-public/voxl-sdk/utilities/mavlink-camera-manager) | 0.1.0 (unchanged) | ✅ |  |
| [qrb5165-dfs-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-dfs-server) | 0.0.5 --> 0.1.0 |  | ✅ |
| [qrb5165-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/qrb5165-imu-server) | 0.2.4 --> 0.5.0 |  | ✅ |
| [qrb5165-system-tweaks](https://gitlab.com/voxl-public/voxl-sdk/utilities/qrb5165-system-tweaks) | 0.1.3 --> 0.1.5 |  | ✅ |
| [qrb5165-tflite](https://gitlab.com/voxl-public/voxl-sdk/third-party/qrb5165-tflite) | 2.8.0-2 (unchanged) |  | ✅ |
| [voxl-bind](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-bind) | 0.0.1 (unchanged) |  | ✅ |
| [voxl-camera-calibration](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-camera-calibration) | 0.2.2 --> 0.2.3 | ✅ | ✅ |
| [voxl-camera-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-camera-server) | 1.1.0 --> 1.3.5 | ✅ | ✅ |
| [voxl-ceres-solver](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-ceres-solver) | 1.14.0-7 --> 1.14.0-9 | ✅ | ✅ |
| [voxl-cpu-monitor](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-cpu-monitor) | 0.2.6 --> 0.3.0 | ✅ | ✅ |
| [voxl-docker-support](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-docker-support) | 1.2.4 (unchanged) | ✅ | ✅ |
| [voxl-eigen3](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-eigen3) | 3.4.0 (new) | ✅ | ✅ |
| [voxl-flir-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-flir-server) | 0.0.4 --> 0.2.0 | ✅ | ✅ |
| [voxl-gphoto2-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-gphoto2-server) | 0.0.10 (unchanged) | ✅ | ✅ |
| [voxl-jpeg-turbo](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-jpeg-turbo) | 2.1.3-4 (unchanged) | ✅ | ✅ |
| [voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libgphoto2) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-libuvc](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-libuvc) | 1.0.6 --> 1.0.7 | ✅ | ✅ |
| [voxl-logger](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-logger) | 0.3.2 --> 0.3.4 | ✅ | ✅ |
| [voxl-mapper](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mapper) | 0.0.7 --> 0.1.5 | ✅ | ✅ |
| [voxl-mavlink](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mavlink) | 0.1.0 (unchanged) | ✅ | ✅ |
| [voxl-mavlink-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-mavlink-server) | 0.1.3 --> 0.2.0 | ✅ | ✅ |
| [voxl-modem](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-modem) | 0.15.2 --> 0.16.1 | ✅ | ✅ |
| [voxl-mongoose](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-mongoose) | 7.6.0 --> 7.7.0-1 | ✅ | ✅ |
| [voxl-mpa-tools](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-tools) | 0.7.2 --> 0.7.6 | ✅ | ✅ |
| [voxl-mpa-to-ros](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-mpa-to-ros) | 0.3.3 --> 0.3.6 | ✅ | ✅ |
| [voxl-nlopt](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-nlopt) | 2.5.0-4 (unchanged) | ✅ | ✅ |
| [voxl-opencv](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-opencv) | 4.5.5-1 (unchanged) | ✅ | ✅ |
| [voxl-portal](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-portal) | 0.2.8 --> 0.4.2 | ✅ | ✅ |
| [voxl-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4) | 1.4.16 --> 1.12.31 |  | ✅ |
| [voxl-px4-imu-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-px4-imu-server) | 0.1.2 (unchanged) |  | ✅ |
| [voxl-qvio-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-qvio-server) | 0.7.1 --> 0.8.2 | ✅ | ✅ |
| [voxl-remote-id](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-remote-id) | 0.0.5 (new) |  | ✅ |
| [voxl-streamer](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-streamer) | 0.3.6 --> 0.4.1 | ✅ | ✅ |
| [voxl-tag-detector](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tag-detector) | 0.0.4 (unchanged) | ✅ | ✅ |
| [voxl-tflite-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-tflite-server) | 0.2.7 --> 0.3.1 | ✅ | ✅ |
| [voxl-utils](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-utils) | 1.1.4 --> 1.2.2 | ✅ | ✅ |
| [voxl-uvc-server](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-uvc-server) | 0.1.1 --> 0.1.3 | ✅ | ✅ |
| [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-sdk/services/voxl-vision-px4) | 1.2.0 --> 1.4.0 | ✅ | ✅ |
| [voxl-voxblox](https://gitlab.com/voxl-public/voxl-sdk/third-party/voxl-voxblox) | 1.0.4 --> 1.1.3 | ✅ | ✅ |
| [voxl-vpn](https://gitlab.com/voxl-public/voxl-sdk/utilities/voxl-vpn) | 0.0.6 (unchanged) | ✅ |  |


