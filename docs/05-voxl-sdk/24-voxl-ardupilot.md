---
title: VOXL ArduPilot 
layout: default
parent: VOXL SDK
has_children: false
nav_order: 24
permalink: /voxl-ardupilot/
---

# VOXL ArduPilot

Initial support for VOXL 2 is in mainline ArduPilot now. This page will explain current status and will be updated as improvements are made.

For advanced / experienced users who see the VOXL 2 (and VOXL 2 mini) support in ArduPilot and want to start playing around with it here are some helpful tips.

So far it has only been tested on four different models of small to medium sized quadcopter. No fixed wing testing (e.g. arduplane) has happened yet although we are making plans to begin that integration soon. However, the VOXL 2 IO board (https://www.modalai.com/products/voxl2-io) is supported now and that provides up to 8 PWM outputs for use with PWM ESCs on quadcopters or PWM servos for fixed wings. The VOXL 2 IO board has been tested on a quadcopter with 4 PWM ESCs. So, theoretically, fixed wings should work with VOXL 2 and the VOXL 2 IO board now.

- Install a recent SDK (1.3.5+)
- Disable the PX4 service
    - `voxl2:~$ systemctl disable voxl-px4`
- Download and install the latest prebuilt ArduPilot QURT Debian package
    - ArduCopter: https://firmware.ardupilot.org/Copter/latest/QURT/
        - For example:
        - `voxl2:~$ wget https://firmware.ardupilot.org/Copter/latest/QURT/voxl-ardupilot_ArduCopter_4.7.0-cb690799_arm64.deb`
        - `voxl2:~$ dpkg -i voxl-ardupilot_ArduCopter_4.7.0-cb690799_arm64.deb`
    - ArduPlane: https://firmware.ardupilot.org/Plane/latest/QURT/
    - ArduRover: https://firmware.ardupilot.org/Rover/latest/QURT/
- Add link to desired default parameters in \data\APM (optional)
    - For example:
    - `voxl2:~$ cd /data/APM`
    - `voxl2:/data/APM$ ln -s D0013.parm defaults.parm`
- Enable the ardupilot service
    - `voxl2:~$ systemctl enable voxl-ardupilot`

There are a few default parameter files available in mainline [here](https://github.com/ArduPilot/ardupilot/tree/master/Tools/Frame_params/ModalAI)

The AutonomyDevKit parameters should also be correct for the [Starling 2](/starling-2/)

Validate all parameter settings. Follow all relevant ArduPilot setup documentation to make
sure that the vehicle is configured properly. Calibrate all sensors and the RC.

You can get the firmware builds directly from the ArduPilot servers or you can build it yourself. In order to build it you will need to procure the Hexagon SDK from Qualcomm and integrate into a build docker. [This project](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-ardupilot-build) will help get that all setup. 

There is more build / setup info on the [ArduPilot repo](https://github.com/ArduPilot/ardupilot/tree/master/libraries/AP_HAL_QURT/ap_host/service)

Here is a forum post with [more information](https://forum.modalai.com/topic/3640/ardupilot-on-voxl2)

Test flight video [here](https://www.youtube.com/watch?v=l6c65-E-lzg)

Test flight video with VOXL 2 IO board and PWM ESCs [here](https://www.youtube.com/watch?v=7sUJ27zkxC4)


