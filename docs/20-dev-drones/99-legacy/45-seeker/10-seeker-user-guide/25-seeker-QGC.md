---
layout: default
title: Connect to QGC
parent: Seeker User Guide
nav_order: 25
has_children: false
permalink: /seeker-user-guide-network/
---

# Connect Seeker to QGC
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


### Edit voxl-mavlink-server.conf

VOXL needs to know the IP address of your computer running qGroundControl. This is set in the 'qgc_ip' field of the `/etc/modalai/voxl-mavlink-server.conf` file on VOXL.  This will inform the `voxl-mavlink-server` systemd service to send MAVLink data to our computer.

Run the following command and update the `qgc_ip` field to match your computer's IP.

```bash
yocto:/home/root# vi /etc/modalai/voxl-mavlink-server.conf
```

![voxl-vi-cmd-line.png](/images/m500/voxl-vi-cmd-line.png)

Hit `i` to enter edit mode, use arrow keys to navigate to the `qgc_ip` field and edit there. To save, hit `esc` then `:wq` to write and quit.

![voxl-vi-edit.png](/images/m500/voxl-vi-edit.png)

If you put VOXL on your own WiFi or LTE network you will have to do the same procedure.

For more information on this config file and the options available see the [voxl-vision-hub manual](/voxl-vision-px4-features/).


### Restart the voxl-mavlink-server Service

To reset the `voxl-mavlink-server` service and reload the configuration, run the following command:

```bash
yocto:/home/root# systemctl restart voxl-mavlink-server
```

Note this will also reset the Visual Inertial Odometry (VIO) algorithm and reset VIO's origin to wherever the drone is when you reset the service.


### Confirm Connection with QGroundControl

After the previous step is completed, the vehicle should automatically connect to QGroundControl

![voxl-qgc-connected.png](/images/m500/voxl-qgc-connected.png)

---

[Next Step: RC Settings](/seeker-user-guide-px4/){: .btn .btn-green }
