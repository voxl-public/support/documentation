---
layout: default
title: RB5 Linux User Guide
parent: Qualcomm Flight RB5 Developer Guide
nav_order: 10
has_children: false
permalink: /Qualcomm-Flight-RB5-linux-user-guide/
search_exclude: true
---

# Qualcomm Flight RB5 Linux User Guide


## UARTs

### /dev/ttyHS0 - (internal use only)

Used internally by the SOM Wi-Fi/BT.

### /dev/ttyHS1 - J12 - RC Input

2 wire UART exposed on J12, by default is used for a RC input for PX4, see [voxl.config](https://github.com/modalai/px4-firmware/blob/voxl-master/boards/modalai/rb5-flight/voxl.config#L114)

### /dev/ttyHS2 - J19 - GNSS Input

2 wire UART exposed on J19, by default is used for a GNNS input for PX4, see [voxl.config](https://github.com/modalai/px4-firmware/blob/voxl-master/boards/modalai/rb5-flight/voxl.config#L101)

### /dev/ttyHS3 - J8 - Camera Connector UART

2 wire UART exposed on the J8 camera connector.

### /dev/ttyHS4 - J3 - B2B pins 3/5

2 wire UART exposed on the J3 board-to-board connector pins 3/5.

Used with add-on boards like M0125 to expose an apps proc UART.

### /dev/ttyHS5 - J5 - HS B2B pins 48/49

2 wire UART exposed on the J board-to-board connector pins 48/49.

Used with add-on boards like M0090, exposing the 2W UART on J9 pins 2/3.
