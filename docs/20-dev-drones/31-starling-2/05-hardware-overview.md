---
layout: default
title: 1. Hardware Overview
parent: Starling 2
nav_order: 05
has_children: false
permalink: /starling-2-hardware-overview/
---

# Starling Hardware Overview
{: .no_toc }

This section contains an overview of the hardware components of the Starling Development Drone.

## Table of Contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

For technical details, see the [datasheet](/starling-2-datasheet/) page.

{: .alert .simple-alert}
**WARNING:** *Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

## Hardware Overview Video

{% include youtubePlayer.html id="M9OiMpbEYOg" %}

## Development Kit Contents

Starling is available in a variety of [kits](https://www.modalai.com/products/starling-2) depending on what you have and need.


## Components


<br>
[Next: Starling Datasheet](/starling-2-datasheet/){: .btn .btn-green }
