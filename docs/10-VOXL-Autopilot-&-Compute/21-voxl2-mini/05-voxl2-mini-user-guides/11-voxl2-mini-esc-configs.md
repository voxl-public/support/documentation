---
layout: default
title: VOXL 2 Mini ESC Configs
parent: VOXL 2 Mini User Guides
nav_order: 11
permalink: /voxl2-mini-esc-configs/
---

# VOXL 2 Mini ESC Configurations
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Summary

We are working to expand the capabilities of VOXL 2 every day. The built-in PX4 flight controller enables industry leading SWAP for an autonomous flight controller, but not every interface and flight controller are supported yet. This page provides an overview of available connectivity. If this connectivity is insufficient for your application, VOXL 2 is a world-class companion computer for autonomous navigation and AI when paired with an [external flight controller](/voxl2-external-flight-controller/).

## ESC Options for Built-in Flight Controller

| Protocol | Driver                                                                                           | Instructions                                               | Max Number of Channels | Example Hardware                                  |
|----------|--------------------------------------------------------------------------------------------------|------------------------------------------------------------|------------------------|---------------------------------------------------|
| UART     | [voxl_esc](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/actuators/voxl_esc) | Below                                                      | 4                      | [VOXL 4-in-1 ESC](/modal-esc-datasheet/)          |
| UART     | [voxl_esc](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/actuators/voxl_esc) | Below                                                      | 4                      | [VOXL Mini 4-in-1 ESC](/voxl-mini-esc-datasheet/) |
| UART     | [voxl_esc](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/actuators/voxl_esc) | Below                                                      | 4                      | [VOXL FPV 4-in-1 ESC](/voxl-fpv-esc-datasheet/)   |
| PWM      | [voxl2_io](https://github.com/modalai/px4-firmware/blob/voxl-dev/src/drivers/voxl2_io)           | [VOXL 2 IO](https://docs.modalai.com/voxl2-io-user-guide/) | 8                      | [VOXL 2 IO](https://docs.modalai.com/voxl2-io/)   |
| DSHOT    | voxl2_io in development                                                                          | Not yet supported                                          | 8                      |                                                   |


**Note:** The modal_io driver was renamed to voxl_esc in SDK 1.1.1+, more information can be found in the [VOXL ESCs section](/docs/70-accessories/75-voxl-escs/20-voxl-esc-px4-integration.md).

## VOXL ESC - 4-in-1

### Hardware Setup  

For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL ESC's (M0049 or M0117) `J2` using [MCBL-00081](https://docs.modalai.com/cable-datasheets/#mcbl-00081) or building your own following this.

<img src="/images/voxl2-mini/m0104-user-guides-esc.jpg" alt="m0104-user-guides-esc.jpg" width="1280"/>

M0049/M0117 J2:
- Connector on board : Hirose DF13A-6P-1.25H
- Mating connector : DF13-6S-1.25C

M0104 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S

#### VBAT Power and Motors

Use standard installation processes following the [datasheet](https://docs.modalai.com/modal-esc-datasheet/) pinouts.

### Software Setup

#### Checking Status

If your voxl2 mini has SDK 1.1.1+ or newer, you can use the `px4-qshell voxl_esc status` command to access the driver which has debug info and tools. If you are on an older SDK, the `px4-qshell modal_io status` command can be used.

The `px4-listener esc_status` can be used to check status of published topics.

####  PX4 Driver

The PX4 parameters that are used by the system are [here](https://docs.modalai.com/modal-esc-px4-user-guide/#px4-params)

For example, if you need to change the motor ordering or min/max RPMs, these are params you will want to tweak.

The driver source code for voxl_esc [is here](https://github.com/modalai/px4-firmware/tree/voxl-dev/src/drivers/actuators/voxl_esc) for reference. The driver source code for modal_io [is here](https://github.com/PX4/PX4-Autopilot/tree/main/src/drivers/actuators/modal_io).

When PX4 runs on VOXL2, [this is where voxl_esc is started](https://github.com/modalai/px4-firmware/blob/voxl-dev/boards/modalai/voxl2/target/voxl-px4-start#L115) using SDK 1.1.1+ and [this is where modal_io is started](https://github.com/PX4/PX4-Autopilot/blob/main/boards/modalai/voxl2/target/voxl-px4-start#L94) on older SDKs.

#### How to Spin Motors

- Use the `actuator_test` command, for example `actuator_test set -m 1 -v 0.05 -t 10`, which spins PX4 motor 1 for 10 seconds
- Use the motor test feature in QGC.

## VOXL Mini ESC - 4-in-1

### Hardware Setup  

For bi-direction UART communications, connect VOXL 2's `J19` connector to the VOXL Mini ESC's (M0129) `J1` using [MCBL-00082](https://docs.modalai.com/cable-datasheets/#mcbl-00082) or building your own following this.

<img src="/images/voxl2-mini/m0104-m0129.jpg" alt="m0104-m0129" width="1280"/>

M0129 J1:
- Connector - 4 Position JST GH, Vertical, BM04B-GHS-TBT
- Mating Connector - JST GHR-04V-S

M0104 J19:
- Connector on board : SM12B-GHS-TB(LF)(SN)
- Mating connector : GHR-12V-S


## PWM ESC Option

PWM ESCs can be used with VOXL 2 and VOXL 2 Mini's built in flight controller. Due to the lack of PWM output on the Qualcomm QRB5165 chipset, an external PWM generator is required. The VOXL 2 ecosystem supports the [VOXL 2 IO](https://docs.modalai.com/voxl2-io/). To use the VOXL 2 IO to control PWM ESCs follow the [VOXL 2 IO User's Guide](https://docs.modalai.com/voxl2-io-user-guide/)