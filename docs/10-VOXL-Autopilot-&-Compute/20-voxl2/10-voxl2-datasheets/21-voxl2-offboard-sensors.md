---
layout: default
title: VOXL 2 Offboard Sensors
parent: VOXL 2 Datasheets
nav_order: 21
permalink:  /voxl2-offboard-sensors/
youtubeId: 2-MWy2tcMJg
---

# VOXL 2 Offboard Sensors
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL2 IO / RC Input

[VOXL 2 IO](/voxl2-io/) provides the ability to extend VOXL 2 with PWM outputs and additional supported RC inputs and can connect here.

<img src="/images/voxl2/m0054-offboard-sensors-rc-2.jpg" alt="m0054-offboard-sensors-rc" width="1280"/>

**J19:** Full pinouts for all the connects are [available here](/voxl2-connectors/).

| Pin # | Signal Name     | Notes                               |
|-------|-----------------|-------------------------------------|
| 1     | | Not used for RC |
| 2     | | Not used for RC |
| 3     | | Not used for RC |
| 4     | | Not used for RC |
| 5     | | Not used for RC |
| 6     | | Not used for RC |
| 7     | | Not used for RC |
| 8     | | Not used for RC |
| 9     | VREG_3P3V_RC            | RC power *, controllable via GPIO 159 |
| 10    | RCIO_UART_TX (Output)   | SSC_QUP7, 3.3V signal levels	|
| 11    | RCIO_UART_RX (Input)    | SSC_QUP7, 3.3V signal levels	|
| 12    | GND             |                                     |

## GPS (GNSS/Mag)

An external GPS (GNSS/Mag) can be connected to VOXL 2's J19 connector.

<img src="/images/voxl2/m0054-offboard-sensors-gnss-mag.png" alt="m0054-offboard-sensors-gnss-mag" width="1280"/>

| Name / Designator            | Description         | Interface                      |
|---                           |---                  |---                             |
| GNSS UART / exposed via J19  | PX4 GNSS            | SSC_QUP6, 2W UART, SLPI (sDSP) | 
| Mag I2C / exposed via J19    | PX4 Mag             | SSC_QUP0, I2C, SLPI (sDSP)     |

**J19:** Full pinouts for all the connects are [available here](/voxl2-connectors/).

| Pin # | Signal Name     | Notes                               |
|-------|-----------------|-------------------------------------|
| 1     | VDC_5V_LOCAL    | GNSS/Mag power *                    |
| 2     | GNSS TX 3P3V    | slpi_proc, SSC_QUP7                 |
| 3     | GNSS RX 3P3V    | slpi_proc, SSC_QUP7                 |
| 4     | MAG SCL 3P3V    | slpi_proc, SSC_QUP1                 |
| 5     | MAG SDA 3P3V    | slpi_proc, SSC_QUP1                 |
| 6     | GND             |                                     |
| 7     |    | Not used for GNSS/Mag |
| 8     |    | Not used for GNSS/Mag |
| 9     |    | Not used for GNSS/Mag |
| 10    |    | Not used for GNSS/Mag |
| 11    |    | Not used for GNSS/Mag |
| 12    |    | Not used for GNSS/Mag |

## Power Monitoring

<img src="/images/voxl2/m0054-offboard-sensors-pm.png" alt="m0054-offboard-sensors-pm" width="1280"/>

Although not technically onboard VOXL 2, using the VOXL Power Module v3 provides the ability to monitor battery voltage and regulated 5VDC voltage from the power module.

| Name / Designator           | Description    | Interface                                | Driver |
|---                          |---             |---                                       |---     |
| PX4 Power Monitoring (J4)   | QTY2 INA231    | SSC_QUP2, I2C, SLPI (sDSP), Addrs: 0x44h and 0x45h | [voxlpm](https://dev.px4.io/master/en/middleware/modules_driver.html#voxlpm) |


**J4:** Full pinouts for all the connects are [available here](/voxl2-connectors/).

| Pin# | Signal   | Notes/Usage                                          |
|------|----------|------------------------------------------------------|
| 1    | VDCIN_5V | DC from Power Module, “unprotected”                  |
| 2    | GND      | Power Module Return                                  |
| 3    | I2C_CLK  | SSC_QUP_1, 5V signal levels, Pullups on Power Module |
| 4    | I2C_SDA  | SSC_QUP_1, 5V signal levels, Pullups on Power Module |

Full pinouts for all the connects are [available here](/voxl2-connectors/).

## External SPIs

The following SPI busses are available from the applications processor.

<img src="/images/voxl2/m0054-offboard-sensors-spi-2.jpg" alt="m0054-offboard-sensors-spi"/>

| Name / Designator         | Description         | Interface        |
|---                        |---                  |---               |
| SPI0 / exposed via J6     | camera group 0 SPI  | /dev/spidev0.0   | 
| SPI1 / exposed via J7     | camera group 1 SPI  | /dev/spidev1.0   |
| SPI14 / exposed via J10   | external SPI        | /dev/spidev14.0  |

Full pinouts for all the connects are [available here](/voxl2-connectors/).

Related video:

{% include youtubePlayer.html id=page.youtubeId %}
