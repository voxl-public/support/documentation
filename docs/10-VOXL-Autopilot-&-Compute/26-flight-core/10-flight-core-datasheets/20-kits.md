---
layout: default
title: Flight Core Kits
parent: Flight Core Datasheets
nav_order: 20
permalink: /flight-core-kits/
---

# Flight Core Kits
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Flight Core Development Kit

![fc-dk](/images/flight-core/fc-dk.jpg)

[Available for purchase here](https://www.modalai.com/collections/voxl-development-kits/products/flight-core)

## Kits available:

- [MDK-M0018-2-00](https://www.modalai.com/products/flight-core-pcb-only?_pos=2&_sid=513cb1197&_ss=r) - Flight Core Board only

- [MDK-M0018-2-01](https://www.modalai.com/products/flight-core?_pos=8&_sid=513cb1197&_ss=r) - Flight Core with Dev kit - PWM breakout (M0022), and cables (USB-to-JST MCBL-00010, VOXL Serial MCBL-00007, RC input MCBL-00005)

- [MDK-M0018-1](https://www.modalai.com/products/flight-core-cable-kit?_pos=1&_sid=513cb1197&_ss=r) - Flight core Cable Kit Only - PWM breakout (M0022), and cables (USB-to-JST MCBL-00010, VOXL Serial MCBL-00007, RC input MCBL-00005)



