---
layout: default
title: Flight Core Datasheets
parent: Flight Core
nav_order: 10
has_children: true
permalink: /flight-core-datasheets/
---

# Flight Core Datasheets
{: .no_toc }

The ModalAI Flight Core is an STM32F7-based flight controller for PX4 and ArduPilot, assembled in the USA.  The Flight Core can be paired with [VOXL](/voxl/) or [VOXL 2](/voxl-2/) for obstacle avoidance and GPS-denied navigation, or used independently as a standalone flight controller.

![fc-top-with-quarter](/images/flight-core/modalai-fc-v1-quarter.jpg)

